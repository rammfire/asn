﻿using Models.Implements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Token
{
    public interface ITokenService
    {
        string BuildToken(string key, string issuer, Client client);
        bool IsTokenValid(string key, string issuer, string token);
    }
}
