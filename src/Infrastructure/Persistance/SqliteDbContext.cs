﻿using Applications.IContextHere;
using Microsoft.EntityFrameworkCore;
using Models.Implements;

namespace Infrastructure.Persistance;

/// <summary>
/// Класс реализует интерфейс для связи с БД
/// </summary>
public class SqliteDbContext : DbContext, ISqliteContext
{
    public DbSet<Client> Clients { get; set; }
    public DbSet<Post> Posts { get; set; }
	public DbSet<Comment> Comments { get; set; }


	public Task<int> SaveChangesAsync(CancellationToken cancelationToken)
	{
		return SaveChangesAsync(cancelationToken);
	}

	public SqliteDbContext()
	{

	}

	public SqliteDbContext(DbContextOptions<SqliteDbContext> options)
					: base(options)
	{

	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);
	}

	protected override void OnConfiguring(DbContextOptionsBuilder options)
	{
		if (!options.IsConfigured)
		{
			options.UseSqlite("Data Source = SuperDatabase.db");
		}
	}
}
