﻿namespace Models.Interface;

/// <summary>
/// Описание интерфейса, который содержит сведения из разных таблиц
/// для отображения комментариев
/// </summary>
public interface ICommentsViewer
{
    /// <summary>
    /// Автор комментария
    /// </summary>
    public string AuthorName { get; set; }

    /// <summary>
    /// АйДи комментария
    /// </summary>
    public int CommentId { get; set; }

    /// <summary>
    /// Содержание комментария
    /// </summary>
    public string CommentContent { get; set; }

    /// <summary>
    /// Можно ли его удалить?
    /// </summary>
    public bool CanDelete { get; set; }
}
