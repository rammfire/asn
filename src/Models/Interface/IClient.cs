using Models.Implements;
namespace Models.Interface;

/// <summary>
/// ��������� ��������� �������� �������
/// </summary>
public interface IClient
{
    /// <summary>
    /// ���
    /// </summary>
    public string Name { get; set;}

    /// <summary>
    /// �������
    /// </summary>
    public string LastName { get; set; }

    /// <summary>
    /// ������
    /// </summary>
    public string Password { get; set; }

    /// <summary>
    /// ����� ��������
    /// </summary>
    public string PhoneNumber { get; set; }

    /// <summary>
    /// ����� ����������� �����
    /// </summary>
    public string Email { get; set; }

    /// <summary>
    /// ���� ��������
    /// </summary>
    public DateTime Birthday { get; set; }

    /// <summary>
    /// ����� �������
    /// </summary>
    public List<Post> CLientPosts { get; set; }
}