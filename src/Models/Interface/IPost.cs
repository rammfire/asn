using Models.Implements;
namespace Models.Interface;

/// <summary>
/// ��������� ��������� ������� ����
/// </summary>
public interface IPost
{
    /// <summary>
    /// ����� � �������� ��������
    /// </summary>
    public Client Author { get; set; }

    /// <summary>
    /// �������� �����
    /// </summary>
    public string Title { get; set; }

    /// <summary>
    /// ����������
    /// </summary>
    public string PostContent { get; set; }

    /// <summary>
    /// �������� ���������
    /// </summary>
    public bool LikeThisPost { get; set; }

    /// <summary>
    /// ������ ������������ � �����
    /// </summary>
    public List<Comment> Comments { get; set; }
}