﻿using Models.Implements;
namespace Models.Interface;

/// <summary>
/// Интерфейс описывает комментарии к постам
/// </summary>
public interface IComment
{

    /// <summary>
    /// Связь с таблицей авторов
    /// </summary>
    public Client Author { get; set; }

    /// <summary>
    /// АйДи автора
    /// </summary>
    public int AuthorId { get; set; }

    /// <summary>
    /// Связь с таблицей постов
    /// </summary>
    public Post Post { get; set; }

    /// <summary>
    /// АйДи поста
    /// </summary>
    public int PostId { get; set; }

    /// <summary>
    /// Нравится ли комментарий
    /// </summary>
    public bool LikeThisComment { get; set; }

    /// <summary>
    /// Текст комментария
    /// </summary>
    public string CommentContent { get; set; }
}
