﻿using System.ComponentModel.DataAnnotations;

namespace Models.ForNewUsers;

/// <summary>
/// Класс описывает второй этап регистрации на портале
/// </summary>
public class StageTwo
{
    /// <summary>
    /// Просим указать имя
    /// </summary>
    [Required(ErrorMessage = "Укажите Ваше имя.")]
    [StringLength(20, ErrorMessage = "Должно быть не длиннее 20 символов")]
    public string Name { get; set; }

    /// <summary>
    /// Просим указать фамилию
    /// </summary>
    [Required(ErrorMessage = "Укажите Вашу фамилию.")]
    [StringLength(50, ErrorMessage = "Должно быть не длиннее 50 символов")]
    public string LastName { get; set; }

    /// <summary>
    /// Дату рождения
    /// </summary>
    public string Birthday { get; set; }
}
