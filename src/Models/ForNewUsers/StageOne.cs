﻿using System.ComponentModel.DataAnnotations;


/// <summary>
/// Класс описывает первый этап регистрации на портале
/// </summary>
namespace Models.ForNewUsers;

public class StageOne
{
    /// <summary>
    /// Просим указать почтовый ящик
    /// </summary>
    [Required(ErrorMessage = "Укажите почтовый ящик.")]
    [RegularExpression(".+@.+\\..+", ErrorMessage = "Ошибка в указании ящика.")]
    public string Email { get; set; }

    /// <summary>
    /// Просим указать номер телефона
    /// </summary>
    [Required(ErrorMessage = "Укажите номер телефона в формате +71112223344")]
    [StringLength(int.MaxValue, MinimumLength = 12, ErrorMessage = "Укажите номер телефона в формате +71112223344")]
    public string PhoneNumber { get; set; }
}
