﻿using CSharpFunctionalExtensions;
using Models.Interface;

namespace Models.Implements;

/// <summary>
/// Класс реализует интерфейс комментариев
/// </summary>
public class Comment : Entity<int>, IComment
{
    public Client Author { get; set; }
    public int AuthorId { get; set; }
    public Post Post { get; set; }
    public int PostId { get; set; }
    public bool LikeThisComment { get; set; }
    public string CommentContent { get; set; }
}
