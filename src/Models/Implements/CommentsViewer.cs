﻿using Models.Interface;

namespace Models.Implements;

/// <summary>
/// Реализация интерфейса, который содержит сведения из разных таблиц
/// для отображения комментариев
/// </summary>
public class CommentsViewer : ICommentsViewer
{
    public string AuthorName { get; set; }
    public int CommentId { get; set; }
    public string CommentContent { get; set; }
    public bool LikeThisComment { get; set; }
    public bool CanDelete { get; set; }

}
