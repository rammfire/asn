using Models.Interface;
using CSharpFunctionalExtensions;
using System.ComponentModel.DataAnnotations;

namespace Models.Implements;

public class Post : Entity<int>, IPost
{
    [Required]
    public Client Author { get; set; }
    [Required]
    public int AuthorId { get; set; }
    [Required]
    public string Title { get; set; }
    [Required]
    public string PostContent { get; set; }
    public bool LikeThisPost { get; set; }
    public List<Comment> Comments { get; set; }
}