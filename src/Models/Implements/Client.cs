using Models.Implements;
using Models.Interface;
using CSharpFunctionalExtensions;
using System.ComponentModel.DataAnnotations;

namespace Models.Implements;

/// <summary>
/// ����� ��������� ��������� �������
/// </summary>
public class Client : Entity<int>, IClient
{
    [Required(ErrorMessage = "������� ���� ���")]
    public string Name { get; set;}
    public string LastName { get; set; }

    [Required(ErrorMessage = "������� ��� ������")]
    public string Password { get; set; }
    public string? PhoneNumber { get; set; }

    [Required(ErrorMessage = "������� ���� �����")]
    public string Email { get; set; }
    public List<Post> CLientPosts { get; set; }
    public DateTime Birthday { get; set; }


}