﻿using Models.Implements;

namespace Models.Pagination;

public class PaginationIndex
{
    public IEnumerable<Post> Posts { get; set; }
    public PageView PageView { get; set; }
}
