﻿using Applications.IContextHere;
using MediatR;
using Microsoft.Extensions.Logging;
using Models.Implements;
using System.Diagnostics;

namespace Applications.Mediatr;

public class RegisterNewUser
{
    public record RegisterNewUserCommand(Client client) : IRequest
    {

    }

    public class Handler : IRequestHandler<RegisterNewUserCommand>
    {
        private readonly ISqliteContext _dataContext;
        private readonly ILogger<Handler> _logger;
        public async Task<Unit> Handle(RegisterNewUserCommand request, CancellationToken cancellationToken)
        {
            _dataContext.Clients.Add(request.client);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }

        public Handler(ISqliteContext sqliteContext, ILogger<Handler> logger)
        {
            _dataContext = sqliteContext;
            _logger = logger;
        }
    }
}
