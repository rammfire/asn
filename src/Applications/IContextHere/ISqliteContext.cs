﻿using Microsoft.EntityFrameworkCore;
using Models.Implements;
namespace Applications.IContextHere;

/// <summary>
/// Интерфейс описывает будущий контекст для связи с БД
/// </summary>
public interface ISqliteContext
{
    /// <summary>
    /// Таблица клиентов
    /// </summary>
    public DbSet<Client> Clients { get; set; }

    /// <summary>
    /// Таблица постов
    /// </summary>
    public DbSet<Post> Posts { get; set; }

    /// <summary>
    /// Таблица комментариев
    /// </summary>
    public DbSet<Comment> Comments { get; set; }

    /// <summary>
    /// Асинхронный метод сохранения данных в БД
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    public Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
}
