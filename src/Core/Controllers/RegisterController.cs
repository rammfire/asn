﻿using Applications.IContextHere;
using Applications.Mediatr;
using Infrastructure.Persistance;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Models.ForNewUsers;
using Models.Implements;
using System.Diagnostics;


namespace Core.Controllers;

/// <summary>
/// Контроллер описывает действия,
/// которые необходимы для успешной регистрации на портале
/// </summary>
public class RegisterController : Controller
{
    private readonly ISqliteContext _context;
    private readonly IMediator _mediator;
    public IActionResult Index()
    {
        return View("BasicDetails");
    }


    /// <summary>
    /// Шаг первый получает данные с представления,
    /// которое собирает начальные данные.
    /// В этом и следующем случае, для хранения промежуточных данных
    /// выбрано свойство TempData, которое автоматически удалит извлечённые данные
    /// </summary>
    /// <param name="data"></param>
    /// <param name="nextBtn"></param>
    /// <returns></returns>
    [HttpPost]
    public ActionResult BasicDetails(StageOne data, string nextBtn)
    {
        if (nextBtn != null)
        {
            // Проверяю валидность данных
            if (ModelState.IsValid)
            {
                TempData["email"] = data.Email;
                TempData["phonenumber"] = data.PhoneNumber;
                return View("MainInformation");
            }
        }
        return View();
    }

    /// <summary>
    /// Действие второго шага регистрации собирет данные
    /// с представления и проверяет их валидность
    /// </summary>
    /// <param name="data"></param>
    /// <param name="nextBtn"></param>
    /// <returns></returns>
    [HttpPost]
    public ActionResult MainInformation(StageTwo data, string nextBtn)
    {
        if (nextBtn != null)
        {
            if (ModelState.IsValid)
            {
                TempData["name"] = data.Name;
                TempData["lastname"] = data.LastName;
                TempData["birthday"] = data.Birthday;
                return View("ConfirmRegistration");
            }
        }
        return View();
    }

    /// <summary>
    /// При подтверждении действия,
    /// все ранее собранные данные собируться в один тип данных - Client.
    /// В дальнейшем они будут помещенны в БД
    /// </summary>
    /// <param name="data"></param>
    /// <param name="nextBtn"></param>
    /// <returns></returns>
    [HttpPost]
    public IActionResult ConfirmRegistration(StageTwo data, string nextBtn)
    {
        if (nextBtn != null)
        {
            Client client = new Client
            {
                Name = (string)TempData["name"],
                LastName = (string)TempData["lastname"],
                Password = "123",
                PhoneNumber = (string)TempData["phonenumber"],
                Email = (string)TempData["email"],
                Birthday = (DateTime)TempData["birthday"]
            };
            
            // Если такой почтовый ящик или номер уже кем то были указаны,
            // то регистрация отменяется 
            bool ifExist = _context.Clients.Any(c => c.Email == client.Email || c.PhoneNumber == client.PhoneNumber);
            if (ifExist)
            {
                RedirectToAction("RegisterFail", "Register");
            }

            // если всё тип-топ, то данные попадают в БД
            else
            {
                using (SqliteDbContext db = new SqliteDbContext())
                {
                    db.Clients.Add(client);
                    db.SaveChanges();
                }
                //_mediator.Send(new RegisterNewUser.RegisterNewUserCommand(client));
                return RedirectToAction("Index", "Home");
            }
        }
        return View();
    }

    /// <summary>
    /// Не нуждается в комментировании
    /// </summary>
    /// <returns></returns>
    public IActionResult RegisterFail()
    {
        return View();
    }
    /// <summary>
    /// Не нуждается в комментировании
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public IActionResult RegistrationSuccess()
    {
        return View();
    }
    public RegisterController(ISqliteContext context, IMediator mediator)
    {
        _context = context;
        _mediator = mediator;
    }
}
