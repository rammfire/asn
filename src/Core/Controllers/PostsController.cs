﻿using Applications.IContextHere;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.Implements;
using Infrastructure.Persistance;
using Models.Pagination;
using System.Diagnostics;

namespace Core.Controllers;

/// <summary>
/// Контроллер реализует возможности для написания постов
/// и их комментирования
/// </summary>
public class PostsController : Controller
{
    private readonly ILogger<PostsController> _logger;
    private readonly ISqliteContext _context;

    /// <summary>
    /// Пагинацирую посты полностью
    /// </summary>
    /// <param name="page"></param>
    /// <returns></returns>
    [Authorize]
    [HttpGet]
    public IActionResult Index(int page = 1)
    {
        var posts = _context.Posts.Where(q => q.AuthorId == (int)HttpContext.Session.GetInt32("CurrentCLientId"));
        var sortPosts =  posts.OrderByDescending(s => s.Id);
        var count = posts.Count();
        int pageSize = count / 2;
        var items = sortPosts.Skip((page - 1) * pageSize).Take(pageSize).ToList();

        PageView pageViewModel = new PageView(count, page, pageSize);
        PaginationIndex viewModel = new PaginationIndex
        {
            PageView = pageViewModel,
            Posts = items
        };
        return View(viewModel);
    }

    /// <summary>
    /// Действие выдаёт страницу,
    /// в которой можно что-то написать
    /// </summary>
    /// <returns></returns>
    [Authorize]
    [HttpGet]
    public IActionResult CreateNewPost()
    {
        return View();
    }

    /// <summary>
    /// Действие получает данные с представления
    /// и записывает их с БД
    /// </summary>
    /// <param name="formalPost"></param>
    /// <returns></returns>
    [Authorize]
    [HttpPost]
    public IActionResult CreateNewPost(Post formalPost)
    {
        Post post = new Post
        {
            AuthorId = (int)HttpContext.Session.GetInt32("CurrentCLientId"),
            Title = formalPost.Title,
            PostContent = formalPost.PostContent,
        };
        using (SqliteDbContext db = new SqliteDbContext())
        {
            db.Posts.Add(post);
            db.SaveChangesAsync();
        }
            return View("SuccessAdded");
    }

    /// <summary>
    /// Действие отвечает за демонстрацию комментариев у поста
    /// </summary>
    /// <param name="formalId"></param>
    /// <returns></returns>
    [Authorize]
    [HttpGet]
    public IActionResult CommentViewer(int formalId)
    {
        /// с помощью Левого вхождения формирую таблицу, собирая данные из двух других
        #region
        var query = from c in _context.Comments.Where(p => p.PostId == formalId)
                    from p in _context.Clients.Where(p => p.Id == c.AuthorId).DefaultIfEmpty()
                    select new
                    {
                        Comments = c.CommentContent,
                        Author = p.Name,
                        AuthorId = c.AuthorId,
                        CommentId = c.Id,
                        LikeThisComment = c.LikeThisComment
                    };
        #endregion
        List<CommentsViewer> tempComments = new List<CommentsViewer>();

        foreach (var item in query)
        {
            if (item.AuthorId == (int)HttpContext.Session.GetInt32("CurrentCLientId"))
            {
                tempComments.Add(new CommentsViewer
                {
                    AuthorName = item.Author,
                    CommentContent = item.Comments,
                    CommentId = item.CommentId,
                    CanDelete = true
                }) ;
            }
            else
            {
                tempComments.Add(new CommentsViewer
                {
                    AuthorName = item.Author,
                    CommentContent = item.Comments,
                    CommentId = item.CommentId,
                    LikeThisComment = item.LikeThisComment
                });
            }
        }
        var sortTempComment = tempComments.OrderByDescending(s => s.CommentId);
        return View(sortTempComment);
    }

    /// <summary>
    /// Загружаю страницу для подтверждения удаления комментария
    /// </summary>
    /// <param name="formalId"></param>
    /// <returns></returns>
    [Authorize]
    [HttpGet]
    public IActionResult ConfirmDeleteComment(int formalId)
    {
        return View(_context.Comments.FirstOrDefault(q => q.Id == formalId));
    }

    /// <summary>
    /// Удаляю комментарий
    /// </summary>
    /// <param name="formalId"></param>
    /// <returns></returns>
    [Authorize]
    [HttpPost]
    public IActionResult ConfirmedDeletionComment(int formalId)
    {
        var request = _context.Comments.FirstOrDefault(q => q.Id == formalId);
        using (SqliteDbContext db = new SqliteDbContext())
        {
            db.Comments.Remove(request);
            db.SaveChanges();
        }
        return RedirectToAction("Index", "Posts");
    }

    /// <summary>
    /// Действие удаляет пост и все комментарий к нему
    /// </summary>
    /// <param name="formalId"></param>
    /// <returns></returns>
    [Authorize]
    [HttpGet]
    public IActionResult DeletePost(int formalId)
    {
        using (SqliteDbContext db = new SqliteDbContext())
        {
            var request = db.Posts.FirstOrDefault(q => q.Id == formalId);
            db.Posts.Remove(request);
            db.SaveChanges();
        }
        return RedirectToAction("Index", "Posts");
    }


    /// <summary>
    /// Действие принимает данные с представления и
    /// помещает данные в БД
    /// </summary>
    /// <param name="comment"></param>
    /// <returns></returns>
    [Authorize]
    [HttpPost]
    public IActionResult CreateComment(Comment comment)
    {
        Comment temp = new Comment
        {
            PostId = (int)HttpContext.Session.GetInt32("OriginalPostId"),
            AuthorId = (int)HttpContext.Session.GetInt32("CurrentCLientId"),
            CommentContent = comment.CommentContent
        };

        Debug.WriteLine(temp.AuthorId + " " + temp.PostId + " " + temp.CommentContent);
        using (SqliteDbContext db = new SqliteDbContext())
        {
            db.Comments.Add(temp);
            db.SaveChanges();
        }
        return RedirectToAction("Index", "Posts");
    }

    /// <summary>
    /// Действие ставит лайк 🔥
    /// </summary>
    /// <param name="formalId"></param>
    /// <returns></returns>
    [Authorize]
    [HttpGet]
    public IActionResult LikeThisPost(int formalId)
    {
        Comment tempComment = _context.Comments.FirstOrDefault(q => q.Id == formalId);
        tempComment.LikeThisComment = true;

        using (SqliteDbContext db = new SqliteDbContext())
        {
            db.Update(tempComment);
            db.SaveChanges();
        }
        return RedirectToAction("Index", "Posts");
    }
    public PostsController(ILogger<PostsController> logger, ISqliteContext context)
    {
        _logger = logger;
        _context = context;
    }
}
