﻿using Applications.IContextHere;
using Infrastructure.Token;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models.Implements;
using Infrastructure.Persistance;

namespace Core.Controllers;

/// <summary>
/// Контроллер описывает действия, которые доступны
/// аутентифицированному клиенту
/// </summary>
public class ClientController : Controller
{
    private readonly IConfiguration _config;
    private readonly ITokenService _tokenService;
    private readonly IMediator _mediator;
    private readonly ILogger<ClientController> _logger;
    private readonly ISqliteContext _context;

    /// <summary>
    /// Действие возвращает Модель,
    /// на основе которой на странице представления будет отображаться
    /// основная информация. Имя клиента и его опубликованные посты
    /// </summary>
    /// <returns></returns>
    [Authorize]
    public IActionResult Index()
    {
        return View(_context.Clients.FirstOrDefault(q => q.Id == HttpContext.Session.GetInt32("CurrentCLientId")));
    }

    /// <summary>
    /// Действие позволяет клиенту изменить информацию о себе
    /// </summary>
    /// <returns></returns>
    [Authorize]
    [HttpPost]
    public IActionResult EditInformationAboutYourSelf()
    {
        return View(_context.Clients.FirstOrDefault(q => q.Id == HttpContext.Session.GetInt32("CurrentCLientId")));
    }

    /// <summary>
    /// Действие перенаправляет клиента на действие,
    /// где он может написать и опубликовать пост
    /// </summary>
    /// <returns></returns>
    [Authorize]
    [HttpPost]
    public IActionResult RedirectToCreateNewPost()
    {
        return RedirectToAction("CreateNewPost", "Posts");
    }

    /// <summary>
    /// Действие подтверждает выбор пользователя
    /// и сохраняет в БД новые данные
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    [Authorize]
    [HttpPost]
    public IActionResult ConfirmNewInformationAboutYourSelf(Client data)
    {
        Client client = _context.Clients.FirstOrDefault(q => q.Id == HttpContext.Session.GetInt32("CurrentCLientId"));
        client.Name = data.Name;
        client.LastName = data.LastName;
        client.PhoneNumber = data.PhoneNumber;
        client.Email = data.Email;
        using (SqliteDbContext sql = new SqliteDbContext())
        {
            sql.Clients.Update(client);
            sql.SaveChangesAsync();
        }
        client = null;
        return RedirectToAction("Index", "Client");
		}

    #region Для теста. Не используется
    [Authorize]
    [HttpGet]
    public IActionResult MainWindow()
    {
        string token = HttpContext.Session.GetString("Token");

        if (token == null)
        {
            return (RedirectToAction("Index"));
        }

        if (!_tokenService.IsTokenValid(_config["Jwt:Key"].ToString(),
            _config["Jwt:Issuer"].ToString(), token))
        {
            return (RedirectToAction("Index"));

        }
        ViewBag.Message = BuildMessage(token, 50);
        return View();
    }

    public IActionResult Error()
    {
        ViewBag.Message = "An error occured...";
        return View();
    }
    private string BuildMessage(string stringToSplit, int chunkSize)
    {
        var data = Enumerable.Range(0, stringToSplit.Length / chunkSize)
            .Select(i => stringToSplit.Substring(i * chunkSize, chunkSize));

        string result = "The generated token is:";

        foreach (string str in data)
        {
            result += Environment.NewLine + str;
        }

        return result;
    }
    #endregion

    public ClientController(ITokenService tokenService, IConfiguration config, IMediator mediator, ILogger<ClientController> logger,
                            ISqliteContext context)
    {
        _tokenService = tokenService;
        _config = config;
        _mediator = mediator;
        _logger = logger;
        _context = context;
    }
}
