﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Core.Models;
using Applications.IContextHere;
using Microsoft.AspNetCore.Authorization;
using Models.Implements;
using Infrastructure.Token;
using Microsoft.AspNetCore.Http;


namespace Core.Controllers;

/// <summary>
/// Контроллер, созданный по умолчанию.
/// </summary>
public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly ISqliteContext _context;
    private readonly ITokenService _tokenService;
    private string generatedToken = null;
    private readonly IConfiguration _config;

    public HomeController(ILogger<HomeController> logger, ISqliteContext context, ITokenService tokenService, IConfiguration config)
    {
        _logger = logger;
        _context = context;
        _tokenService = tokenService;
        _config = config;

    }

    public IActionResult Index()
    {
        return View();
    }

    /// <summary>
    /// Разрешаю не авторизированным пользователям подключаться к представлению.
    /// Собираю данные с представления для авторизации.
    /// </summary>
    /// <param name="login"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    [AllowAnonymous]
    [HttpPost]
    public IActionResult Login(string login, string password)
    {
        // Примитивная проверка данных
        if (string.IsNullOrEmpty(login) || string.IsNullOrEmpty(password))
        {
            return (RedirectToAction("Index"));
        }
        
        Client client = new Client { Email = login, PhoneNumber = login, Password = password };
        IActionResult response = Unauthorized();
        var validUser = GetUser(client);

        if (validUser != null)
        {
            // Пробую авторизоваться с использованием JWT
            try
            {
                generatedToken = _tokenService.BuildToken(_config["Jwt:Key"].ToString(), _config["Jwt:Issuer"].ToString(),validUser);
                if (generatedToken != null)
                {
                    HttpContext.Session.SetString("Token", generatedToken);
                    return RedirectToAction("Index", "Client");
                }
                else
                {
                    return (RedirectToAction("Error"));
                }

            }
            // Если такой пользователь в базе не найден,
            // перенаправляю на действие Регистрации
            catch (System.ArgumentNullException)
            {
                return RedirectToAction("Index", "Register");
            }

        }
        else
        {
            return (RedirectToAction("Error"));
        }
    }

    /// <summary>
    /// Нахожу в базе пользователя по указанным данным
    /// </summary>
    /// <param name="userModel"></param>
    /// <returns></returns>
    private Client GetUser(Client userModel)
    {
        Client Temp = new Client();
        var request = _context.Clients.Where(e => e.Email == userModel.Email || e.PhoneNumber == userModel.PhoneNumber && e.Password == userModel.Password);
        foreach (var item in request)
        {
            // Важная для дальнейшей работы строчка.
            // Если пользователь успешно аутентифицировался,
            // то записываю на время сессии АйДи клиента
            HttpContext.Session.SetInt32("CurrentCLientId", item.Id);
            Temp = new Client
            {
                Name = item.Name,
            };
        }
        return Temp;
    }

    public IActionResult Privacy()
    {
        return View();
    }
}
