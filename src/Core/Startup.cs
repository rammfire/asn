﻿using Applications.IContextHere;
using Infrastructure.Persistance;
using Infrastructure.Token;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Models.Implements;
using Models.Interface;
using System.Text;
using MediatR;
using Applications.Mediatr;
using AspNetCore.Yandex.ObjectStorage.Extensions;

namespace Core;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;

    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllersWithViews();
        services.AddTransient<ITokenService, TokenService>();
        services.AddTransient<ISqliteContext, SqliteDbContext>();
        services.AddTransient<IClient, Client>();
        services.AddTransient<IPost, Post>();
        services.AddMediatR(typeof(RegisterNewUser.RegisterNewUserCommand).Assembly);

        var sqlstring = Configuration.GetConnectionString("DefaultConnection");
        services.AddDbContext<SqliteDbContext>(options =>
        {
            options.UseSqlite(sqlstring);
        });
        services.AddAuthentication(auth =>
        {
            auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        })
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["Jwt:Issuer"],
                    ValidAudience = Configuration["Jwt:Issuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                };
            });
        services.AddSession(options =>
        {
            options.IdleTimeout = TimeSpan.FromMinutes(30);//We set Time here 
            options.Cookie.HttpOnly = true;
            options.Cookie.IsEssential = true;
        });
        services.AddYandexObjectStorage(options =>
        {
            options.BucketName = "testavatartsbucket";
            options.AccessKey = "YCAJEPi0GguaqIasIgl2jQQQN";
            options.SecretKey = "YCO8Uhib0CumTPxQUZiw8JKZGLN3zsO-JDanCKBH";
        });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandler("/Home/Error");
        }

        app.UseSession();

        app.Use(async (context, next) =>
        {
            var token = context.Session.GetString("Token");
            if (!string.IsNullOrEmpty(token))
            {
                context.Request.Headers.Add("Authorization", "Bearer " + token);
            }
            await next();
        });

        app.UseStaticFiles();
        app.UseRouting();
        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllerRoute(
                name: "default",
                pattern: "{controller=Home}/{action=Index}/{id?}");
            
            endpoints.MapControllerRoute(
                name: "Register",
                pattern: "{controller=Register}/{action=Index}/{id?}");
           
            endpoints.MapControllerRoute(
                name: "Posts",
                pattern: "{controller=Posts}/{action=Index}/{id?}");
        });
    }
}

